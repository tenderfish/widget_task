<?php
namespace danjelleyTests;

require __DIR__ . '/../vendor/autoload.php';

use \danjelley\WidgetFulfillmentProviderFile;

class WidgetFulfillmentProviderFileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider testFulfillDataProvider
     */
    public function testFulfillReturnsCorrectWidgetArray($orderValue, $expectedReturn)
    {
        $fulfiller = new WidgetFulfillmentProviderFile();

        $return = $fulfiller->fulfil($orderValue);

        $this->assertEquals($return, $expectedReturn);
    }

    public function testFulfillDataProvider()
    {
        return array(
            array(0, array()),
            array(1, array(
                    '250' => 1,
                )
            ),
            array(250, array(
                    '250' => 1,
                )
            ),
            array(251, array(
                    '500' => 1,
                )
            ),
            array(451, array(
                    '500' => 1,
                )
            ),
            array(4999, array(
                    '5000' => 1
                )
            ),
            array(501, array(
                    '500' => 1,
                    '250' => 1,
                )
            ),
            array(7001, array(
                    '5000' => 1,
                    '2000' => 1,
                    '250' => 1,
                )
            ),
            array(12001, array(
                    '5000' => 2,
                    '2000' => 1,
                    '250' => 1
                )
            ),
            array(9999, array(
                    '5000' => 2
                )
            )
        );
    }
}
