<?php
namespace danjelley;

// Using an interface makes it easy to swap out the file-based order sizes provider for a db-based order sizes provider later
interface IWidgetFulfillmentProvider
{
    public function getOrderSizes();
    public function fulfil($order);
}
