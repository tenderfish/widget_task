<?php
namespace danjelley;

class WidgetFulfillmentProviderFile implements IWidgetFulfillmentProvider
{
    // ensure $orderSizes is correctly ordered
    private $orderSizes = array(250, 500, 1000, 2000, 5000);
    
    public function fulfil($order)
    {
        if (!$order) {
            return array();
        }
        
        $orderSizes = $orderSizesDesc = $this->getOrderSizes();
        rsort($orderSizesDesc);
        
        $widgetsToReturn = array();
        
        for ($i = 0; $i < count($orderSizesDesc); $i++) {
            // Check if this size fits
            if ($order / $orderSizesDesc[$i] >= 1) {
                // Add the correct number of the widget size that fits :
                $widgetsToReturn[$orderSizesDesc[$i]] = isset($widgetsToReturn[$orderSizesDesc[$i]]) ? $widgetsToReturn[$orderSizesDesc[$i]] + floor($order / $orderSizesDesc[$i]) : floor($order / $orderSizesDesc[$i]);
                
                // Deduct the right number from the order so we fulfil the right amount next time in the loop
                $order -= $orderSizesDesc[$i] * floor($order / $orderSizesDesc[$i]);
            } elseif ($i && $order / $orderSizesDesc[$i - 1] >= 1) {
                $proposedNum = floor($order / $orderSizesDesc[$i - 1]);
                
                // If the next size down would end up using too many packs, optimise:
                if ($proposedNum > 1 && $proposedNum * $orderSizesDesc[$i - 1] >= $orderSizesDesc[$i]) {
                    $widgetsToReturn[$orderSizesDesc[$i]] = isset($widgetsToReturn[$orderSizesDesc[$i]]) ? $widgetsToReturn[$orderSizesDesc[$i]] + 1 : 1;
                    
                    $order -= $orderSizesDesc[$i];
                }
            }
        }
        
        if ($order) {
            // We still have some 'left over', so figure out the right widget size to fulfil.
            foreach ($orderSizes as $orderSize) {
                if ($order <= $orderSize) {
                    $widgetsToReturn[$orderSize] = isset($widgetsToReturn[$orderSize]) ? $widgetsToReturn[$orderSize] + 1 : 1;
                    
                    $order -= $orderSize;
                    break;
                }
            }
        }

        $this->optimiseInitialPass($widgetsToReturn);
        $this->secondOptimisationPass($widgetsToReturn);

        return $widgetsToReturn;
    }

    public function optimiseInitialPass(&$widgetsToReturn)
    {
        $orderSizes = $this->getOrderSizes();
        // Loop over widgetsToReturn to optimise number of packets - highly dependent on keys in $orderSizes
        foreach ($widgetsToReturn as $size => $number) {
            if ($number > 1) {
                // If there is an array key in $orderSizes matching the total
                if (array_search(($number * $size), $orderSizes)) {
                    unset($widgetsToReturn[$size]);
                    
                    $widgetsToReturn[($number * $size)] = isset($widgetsToReturn[($number * $size)]) ? $widgetsToReturn[($number * $size)] + 1 : 1;

                    $this->optimiseInitialPass($widgetsToReturn);
                }
            }
        }
    }

    // This function definitely 'feels' wrong. It loops over the proposed solution, adding each subtotal to the next size
    // up and determines whether this equates to a larger key.
    public function secondOptimisationPass(&$widgetsToReturn)
    {
        $orderSizes = $this->getOrderSizes();
        
        $keys = array_keys($widgetsToReturn);
        for ($i = 0; $i < count($keys) -1; $i++) {
            $proposedNum = $widgetsToReturn[$keys[$i]] * $keys[$i] + $widgetsToReturn[$keys[$i+1]] * $keys[$i+1];
            if (array_search($proposedNum, $orderSizes)) {
                unset($widgetsToReturn[$keys[$i]]);
                unset($widgetsToReturn[$keys[$i+1]]);
                $widgetsToReturn[$proposedNum] = isset($widgetsToReturn[$proposedNum])?$widgetsToReturn[$proposedNum] + 1:1;
            }
        }
    }
    
    public function getOrderSizes()
    {
        return $this->orderSizes;
    }
}
