<?php
include('../vendor/autoload.php');

$fulfiller = new danjelley\WidgetFulfillmentProviderFile();

if (isset($_GET['order'])) {
    $order = (int)abs($_GET['order']);
    $html = "<pre><strong>Order Size " . $order . " : </strong>\n" . print_r($fulfiller->fulfil($order), 1) . "</pre>";
} else {
    $html = "Example results from task spec: <pre>";

    $html .= "<strong>Order Size 1: </strong> \n";
    $html .= print_r($fulfiller->fulfil(1), 1);

    $html .= "<strong>Order Size 250 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(250), 1);

    $html .= "<strong>Order Size 251 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(251), 1);

    $html .= "<strong>Order Size 451 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(451), 1);

    $html .= "<strong>Order Size 501 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(501), 1);

    $html .= "<strong>Order Size 7001 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(7001), 1);

    $html .= "<strong>Order Size 12001 : </strong> \n";
    $html .= print_r($fulfiller->fulfil(12001), 1);

    $html .= "</pre>";
}


?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="css/style.css">

    <script src="js/libs/modernizr-2.0.6.min.js"></script>
</head>
<body>

    <div id="header-container">
        <header class="wrapper clearfix">
            <h1 id="title">Wally's Widgets</h1>
            <nav>
                <ul>
                    <li><a href="#">Home</a></li>
                </ul>
            </nav>
        </header>
    </div>
    <div id="main-container">
        <div id="main" class="wrapper clearfix">
            
            <article>
                <header>
                    <h1>Wally's Widgets Developer Challenge</h1>
                    <p>Please enter an order amount in the form below :</p>
                </header>
                <section>
                    <form action='' method='GET' class="wally">
                        <input type="text" name="order">
                        <button type='submit'>Submit</button>
                    </form>
                </section>
                <section>
                    <h3>Data format : </h3>
                    <pre>array(
    {widget size} => {number of widgets}
)
                    </pre>
                </section>
            </article>
            
            <aside>
                <h3>Results :</h3>
                <p><?php echo $html; ?></p>
            </aside>
            
        </div> <!-- #main -->
    </div> <!-- #main-container -->

    <div id="footer-container">
        <footer class="wrapper">
            <h3>&copy; Wally's Widgets 2017</h3>
        </footer>
    </div>

</body>
</html>

